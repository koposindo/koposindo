@extends($view_path.'.layouts.master')
@push('css')
	<link rel="stylesheet" href="{{asset('components/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" type="text/css">
@endpush
@section('content')
<!-- BEGIN CONTENT -->
<div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
        <div class="row">
            <form action="{{url($path.'/ext/files')}}" method="post" enctype="multipart/form-data">
            	@include('admin.includes.errors')       
                {!!view($view_path.'.builder.select',['id' => 'crud', 'name' => 'crud', 'label' => 'Action', 'data' => [1 => 'Insert', 2 => 'Update'] ,'value' => 1,'form_class' => 'col-md-12'])!!}
                <div class="form-group form-md-line-input col-md-12">
                    <label>File</label><br>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn yellow btn-file">
                            <span class="fileinput-new"> Select File </span>
                            <span class="fileinput-exists"> Change </span>
                            <input type="file" name="jukir"> </span>
                        <span class="fileinput-filename"> </span> &nbsp;
                        <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a> &nbsp;
                        <button type="submit" name="action" value="import" class="fileinput-exists btn green"><i class="fa fa-check"></i> Import</button>
                    </div><br>
                    <a href="{{asset('components/back/files/format-jukir.xlsx')}}" class="btn blue"><i class="fa fa-floppy-o"></i> Download File Contoh</a>
                	<br/><br/>
                	<b>Format: xlsx</b>
                    @if(session('error_row'))
                        <div class="clearfix"></div><hr/>
                        <div class="col-md-10 col-md-offset-2">
                        	<table class="table table-bordered">
                        		<th>Baris</th>
                        		<th>Keterangan</th>
                        		<tbody>
                        			@foreach(session('error_row') as $eq => $ev)
		                        		<tr>
		                        			<td>{{$eq}}</td>
		                        			<td>
		                        				@if(isset($ev['place']))
		                        					Place tidak ditemukan di database
		                        					<br/>
		                        				@endif
		                        				@if(isset($ev['required']))
		                        					Kolom name, password, email, place dan status tidak boleh kosong
		                        					<br/>
		                        				@endif
		                        				@if(isset($ev['email']))
		                        					Email {{$ev['email_value']}} sudah ada dan tidak boleh sama
		                        					<br/>
		                        				@endif
		                        				@if(isset($ev['usernotfound']))
		                        					Email {{$ev['usernotfound_value']}} tidak ditemukan
		                        					<br/>
		                        				@endif
		                        			</td>
		                        		</tr>
			                    	@endforeach
                        		</tbody>
                        	</table>
                        </div>
                    @endif
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>
</div>
@push('scripts')
	<script src="{{asset('components/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}"></script>
@endpush
@endsection
