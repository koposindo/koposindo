@extends($view_path.'.layouts.master')
@push('css')
  <style>
    .table-form .form-control{
      border-bottom: none !important;
      height: initial !important;
      padding: initial !important;
    }

    .table-form .form-group.form-md-line-input{
      margin-bottom: 0 !important;
    }

    .cinema_col{
      min-width: 305px;
    }
  </style>
@endpush
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$ticket->id}}" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">
        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'ticket_name','label' => 'Name','value' => (old('ticket_name') ? old('ticket_name') : $ticket->ticket_name),'attribute' => 'required autofocus','form_class' => 'col-md-6'])!!}

        <div class="clearfix"></div>

        {!!view($view_path.'.builder.textarea',['name' => 'synopsis','label' => 'Synopsis','value' => (old('synopsis') ? old('synopsis') : $ticket->synopsis),'attribute' => 'required autofocus','form_class' => 'col-md-6'])!!}

        <div class="clearfix"></div>

        {!!view($view_path.'.builder.file',['name' => 'image','label' => 'Cover','value' => $ticket->image,'type' => 'file','file_opt' => ['path' => 'components/both/images/movie/'],'upload_type' => 'single-image','class' => 'col-md-12','note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 1280 x 720 px','form_class' => 'col-md-12', 'required' => 'n'])!!}

        {!!view($view_path.'.builder.select',['id' => 'cinema_type','name' => 'cinema_type','label' => 'Cinema Type','value' => (old('type_option') ? old('type_option') : '1'),'attribute' => 'required','form_class' => 'col-md-4', 'data' => $cinema_service, 'class' => 'select2', 'onchange' => ''])!!}

        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <label class="control-label">Cinema Time</label>
            <div class="input-group date form_datetime">
                <input type="text" size="16" readonly class="form-control" id="cinema_time">
                <span class="input-group-btn">
                    <button class="btn default date-set" type="button">
                        <i class="fa fa-calendar"></i>
                    </button>
                </span>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <label class="control-label">&nbsp;</label>
            <div class="input-group">
                <input type="button" class="btn btn-success" onclick="addSchedule()" value="Add">
            </div>
          </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12">
          <div class="table-scrollable">
            <table id="schedule" class="table table-bordered table-form">
              <thead>
                <tr class="parent">
                  <td rowspan="2" class="cinema_col" align="center"><b>Cinema</b></td>

                  @foreach($cinema_date as $cd)
                    <td align="center" id='date-{{$cd->cinema_service_id}}-{{date( "dmY", strtotime($cd->cinema_date) )}}' colspan="{{$cinema_tickets->where('cinema_date', $cd->cinema_date)->where('cinema_service_id', $cd->cinema_service_id)->count()}}">{{date( 'd/m/Y', strtotime($cd->cinema_date) )}} ({{$cd->cinema_service->name}})</td>
                  @endforeach
                </tr>

                <tr class="parent2">
                @foreach($cinema_tickets as $ct)
                  <td align="center" class='time-{{$ct->cinema_service_id}}-{{date( "dmY", strtotime($ct->cinema_date) )}}  showtime-{{$ct->cinema_service_id}}-{{date( "dmY", strtotime($ct->cinema_date) )}}{{str_replace(":", "", substr($ct->cinema_time, 0, 5))}}'>{{substr($ct->cinema_time, 0, 5)}}</td>
                @endforeach
                </tr>
              </thead>

              <tbody>
                @foreach($cinema as $c)
                  <tr id="cinema_{{$c->id}}" valign="middle">
                    <td>{{$c->bioskop_name}}</td>

                    @if(count($tickets->where('cinema_id', $c->id)) > 0)
                      @foreach($tickets->where('cinema_id', $c->id) as $t)
                        <td class='qtywrapper-{{$t->cinema_service_id}}-{{date( "dmY", strtotime($t->cinema_date) )}}'>
                          <div class='form-group form-md-line-input'>
                            <input name='qty[]' class='form-control text-center' value='{{$t->total_ticket}}'>
                            <input type='hidden' name='cinema[]' value='{{$c->id}}'>
                            <input type='hidden' name='showtime[]' value='{{date( "d/m/Y", strtotime($t->cinema_date) )." - ".substr($t->cinema_time, 0, 5)}}'>
                            <input type='hidden' name='service[]' value='{{$t->cinema_service_id}}'>
                          </div>
                        </td>
                      @endforeach
                    @else
                      @foreach($cinema_tickets as $ct)
                        <td class='qtywrapper-{{$ct->cinema_service_id}}-{{date( "dmY", strtotime($ct->cinema_date) )}}'>
                          <div class='form-group form-md-line-input'>
                            <input name='qty[]' class='form-control text-center' value='0'>
                            <input type='hidden' name='cinema[]' value='{{$c->id}}'>
                            <input type='hidden' name='showtime[]' value='{{date( "d/m/Y", strtotime($ct->cinema_date) )." - ".substr($ct->cinema_time, 0, 5)}}'>
                            <input type='hidden' name='service[]' value='{{$ct->cinema_service_id}}'>
                          </div>
                        </td>
                      @endforeach
                    @endif
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>

        <div class="col-md-12 actions margin-top-20">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
        </div>
    </div>
  </div>
</form>
@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      $(".form_datetime").datetimepicker({
          autoclose: true,
          isRTL: App.isRTL(),
          format: "dd/mm/yyyy - hh:ii",
          pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
          minuteStep: 1
      });
    });

    function addSchedule(){
      var cinemaTime = $("#cinema_time").val();
      var cinemaType = $("#cinema_type").select2('data');

      if(cinemaTime == ""){
        alert("Cinema time is required")
      }
      else if($(".showtime-"+cinemaType[0].id+"-"+cinemaTime.replace(/[\/]/g, "").replace(":", "").replace(" - ", "")).length){
        alert("Cinema time already exist")
      }
      else{
        if($('#date-'+cinemaType[0].id+'-'+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")).length){
          var colspan = parseInt($('#date-'+cinemaType[0].id+'-'+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")).attr('colspan'));
          var nextColspan = parseInt($('#date-'+cinemaType[0].id+'-'+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")).attr('colspan')) + 1;
          $('#date-'+cinemaType[0].id+'-'+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")).attr('colspan', nextColspan);

          var append =  "<td align='center' class='time-"+cinemaType[0].id+"-"+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")+" showtime-"+cinemaType[0].id+"-"+cinemaTime.replace(/[\/]/g, "").replace(":", "").replace(" - ", "")+"'>"+cinemaTime.split(" - ")[1]+"</td>";

          $(append).insertAfter('td.time-'+cinemaType[0].id+"-"+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")+":last");

          @foreach($cinema as $c)
            var append2 = "<td class='qtywrapper-"+cinemaType[0].id+"-"+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")+"'>"+
              "<div class='form-group form-md-line-input'>"+
               "<input name='qty[]' class='form-control text-center'>"+
               "<input type='hidden' name='cinema[]' value='{{$c->id}}'>"+
               "<input type='hidden' name='showtime[]' value='"+cinemaTime+"'>"+
               "<input type='hidden' name='service[]' value='"+cinemaType[0].id+"'>"+
              "</div>"+
            "</td>";
            console.log("test");
            $(append2).insertAfter("#schedule tr#cinema_{{$c->id}} td.qtywrapper-"+cinemaType[0].id+"-"+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")+":last");
          @endforeach
        }
        else{
          var append3 = "<td align='center' id='date-"+cinemaType[0].id+"-"+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")+"' colspan='1'>"+
            cinemaTime.split(" - ")[0]+" ("+cinemaType[0].text+")</td>";

          $("#schedule thead tr.parent").append(append3);

          if($(".parent2").length){
            var append = "<td align='center' class='time-"+cinemaType[0].id+"-"+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")+" showtime-"+cinemaType[0].id+"-"+cinemaTime.replace(/[\/]/g, "").replace(":", "").replace(" - ", "")+"'>"+cinemaTime.split(" - ")[1]+"</td>";

            $("#schedule thead tr.parent2").append(append);
          }
          else{
            var append =
                "<tr class='parent2'>"+
                  "<td align='center' class='time-"+cinemaType[0].id+"-"+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")+" showtime-"+cinemaType[0].id+"-"+cinemaTime.replace(/[\/]/g, "").replace(":", "").replace(" - ", "")+"'>"+cinemaTime.split(" - ")[1]+"</td>"+
                "</tr>";

            $("#schedule thead").append(append);
          }

          @foreach($cinema as $c)
            var append2 = "<td class='qtywrapper-"+cinemaType[0].id+"-"+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")+"'>"+
              "<div class='form-group form-md-line-input'>"+
               "<input name='qty[]' class='form-control text-center'>"+
               "<input type='hidden' name='cinema[]' value='{{$c->id}}'>"+
               "<input type='hidden' name='showtime[]' value='"+cinemaTime+"'>"+
               "<input type='hidden' name='service[]' value='"+cinemaType[0].id+"'>"+
              "</div>"+
            "</td>";

            $("#schedule tr#cinema_{{$c->id}}").append(append2);
          @endforeach
        }
      }
    }

  </script>
@endpush
@endsection
