@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/update" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green">
          <i class="icon-layers font-green title-icon"></i>
          <span class="caption-subject bold uppercase"> {{$title}}</span>
        </div>

        <div class="actions">
        <div class="actions">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <datalist></datalist>
      @include('admin.includes.errors')
      <div class="tabbable-line">
          <div class="row">
            <div class="form-group col-md-12 form-group-promo">
              <label for="tag" class="sub-title">Ad Booth</label>
            </div>
            @foreach($data as $s)
            <div class="form-group form-md-line-input col-md-12">
                <!-- <label>{{$s->name}}</label><br> -->
                
                {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'adbooth[]','label' => $s->name,'value' => $s->content,'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}
            </div>
            @endforeach
            </hr>
            <hr/>
          <div class="row">
            <div class="col-md-12 actions">
              {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
            </div>
          </div>
          
      </div>
    </div>
  </div>
</form>
@push('custom_scripts')
  @if ($role->view == 'n')
    <script>
      $(document).ready(function(){
        // $('input,select,textarea').prop('disabled',true);
      });
    </script>
  @endif
@endpush
@endsection
