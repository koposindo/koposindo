<!-- BEGIN HEADER -->
        <div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
                <div class="container">
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler"></a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                </div>

                <!-- BEGIN HEADER MENU -->
            <div class="page-header-menu">
                <div class="container">
                    <!-- BEGIN HEADER SEARCH BOX -->
                    <!-- <form class="search-form" action="page_general_search.html" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" name="query">
                            <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit">
                                    <i class="icon-magnifier"></i>
                                </a>
                            </span>
                        </div>
                    </form> -->
                    <!-- END HEADER SEARCH BOX -->
                    <!-- BEGIN MEGA MENU -->
                    <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                    <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                    <div class="hor-menu  ">
                        <ul class="nav navbar-nav">
                            <li class="menu-dropdown classic-menu-dropdown active" id="cek-alamat">
                                <a href="{{url('/')}}"> Cek Alamat
                                    <!-- <span class="arrow"></span> -->
                                </a>
                                <!-- <ul class="dropdown-menu pull-left">
                                    <li class=" ">
                                        <a href="index.html" class="nav-link  ">
                                            <i class="icon-bar-chart"></i> Default Dashboard
                                            <span class="badge badge-success">1</span>
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="dashboard_2.html" class="nav-link  ">
                                            <i class="icon-bulb"></i> Dashboard 2 </a>
                                    </li>
                                    <li class=" ">
                                        <a href="dashboard_3.html" class="nav-link  ">
                                            <i class="icon-graph"></i> Dashboard 3
                                            <span class="badge badge-danger">3</span>
                                        </a>
                                    </li>
                                </ul> -->
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown" id="about">
                                <a href="{{url('/')}}"> About
                                    <!-- <span class="arrow"></span> -->
                                </a>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown" id="contactt">
                                <a href="{{url('/')}}"> Contact
                                    <!-- <span class="arrow"></span> -->
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MEGA MENU -->
                </div>
            </div>
            </div>
            <!-- END HEADER TOP -->
            
            <!-- END HEADER MENU -->

            <div class="container">
                 <!-- BEGIN LOGO -->
                    <div class="page-logo" style="text-align: center;">
                        <a href="{{url('/')}}">
                            <img src="{{asset('components/both/images/web/Logo-Koposindo.png')}}" alt="logo" class="img-responsive"style="display: unset;">
                        </a>
                    </div>
                    <!-- END LOGO -->
            </div>
        </div>

