@extends($view_path.'.layouts.master')

@section('content')
<style>
	@media all and (orientation:portrait) and (max-width: 480px)
	{
		table, thead, tbody, th, td, tr { 
		display: block; 
		}

		thead tr { 
			position: absolute;
			top: -9999px;
			left: -9999px;
		}
		
		tr { border-top: 1px solid #ccc; }
		
		td { 
			border: none;
			border-bottom: 1px solid #eee; 
			position: relative;
			padding-left: 50%; 
		}
		
		td:before { 

			position: absolute;
			top: 6px;
			left: 6px;
			width: 45%; 
			padding-right: 10px; 
			white-space: nowrap;
		}

		/*td:nth-of-type(1):before { content: "Propinsi"; }
		td:nth-of-type(2):before { content: "Kota/Kab.2"; }
		td:nth-of-type(3):before { content: "Kecamatan"; }
		td:nth-of-type(4):before { content: "Kelurahan"; }
		td:nth-of-type(5):before { content: "Kode Pos"; }*/		
	}


	.table>tbody>tr>td{
		border-bottom: 1px solid #e7ecf1;
	}
</style>

<div class="container">
	    <div class="row search-box">
	    	<div class="col-md-12 col-sm-12">
	    	 	<form action="cek_alamat" method="GET">
		  		<!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
	            	<div class="input-group">
	                	<input type="text" placeholder="Ketik Nama Kota / Kecamatan / Kelurahan / Kodepos.." name="search" class="form-control" required="true">
	                	<span class="input-group-btn">
	                    	<button class="btn btn-cari" type="submit">Cari</button>
	                	</span>
	            	</div>
	        	</form>
           	</div>
	    </div>

	    <div class="row">
	      	<div class="col-md-12 col-sm-12">
	            <div class="portlet light bordered">
	                <div class="portlet-title">
	                    <div class="caption">
	                        <!-- <i class="icon-share font-blue"></i> -->
	                        <h1><span class="caption-subject font-greens bold uppercase">Kode Pos Provinsi Indonesia</span></h1>
	                    </div>
	                    <div class="actions">
	                    </div>
	                </div>
	                <div class="portlet-body">
	                    <div class="scroller" style="" data-always-visible="1" data-rail-visible="0">
					    <div class="row">
							<div class="col-md-12 col-sm-12">
		    					<div class="bilik-iklan-1">
								Bilik Iklan 1
									<!-- <input type="hidden" value="{{$adbooth_1->content}}" id="adbooth_1"></input> -->
		    					</div>
		   				 	</div>
		  			  	</div>
							<br/>
	                        <div class="table-responsive">
	                        	@if($custom_err == NULL)
		                            <table class="table table-striped">
		                                <!-- thead>
		                                    <tr>
		                                        <th> # </th>
		                                        <th> Name </th>
		                                        <th> Total Order Success</th>
		                                    </tr>
		                                </thead> -->
		                                <tbody>
		                                	@foreach($provinsi as $key => $val)
		                                		@php
		                                			$slug 	= str_replace(" ","-",strtolower($val->name));
		                                		@endphp
		                                		@if(($key)%3 == 0 )
		                                			<tr>
		                                		@endif
		                                			<td><a href="provinsi/{{$slug}}">{{$val->name}}</a></td>
		                                	@endforeach
		                                </tbody>
		                            </table>
		                        @else
		                        	@if($custom_err == '404')
		                        		<b>"Halaman tidak ditemukan, Gunakan kotak pencarian untuk menemukan halaman yang dimaksud"</b>
		                        	@endif
		                        @endif
	                        </div>
	                    </div>
	                </div>


	            </div>
	        </div>
	    </div>

	    <div class="row">
			<div class="col-md-12 col-sm-12">
	    		<div class="bilik-iklan-2">
	    			Bilik Iklan 2
	    			<!-- <input type="hidden" value="{{$adbooth_2}}" id="adbooth_2"></input> -->
	    		</div>
	    	</div>
	    </div>

	    <div class="row">
	    	<!-- @foreach ($feed->get_items(0, 5) as $item)
 
				<div class="item">
					<h2 class="title"><a href="{{$item->get_permalink()}}">{{$item->get_title()}}</a></h2>
					{{$item->get_description()}}
					<p><small>Posted on {{$item->get_date('j F Y | g:i a')}}</small></p>
				</div>
		 
			@endforeach -->

			<!-- <div class="col-md-12 col-sm-12">
                <div class="portlet light ">
                    <div class="portlet-title tabbable-line">
                        <div class="caption">
                            <i class="icon-globe font-green-sharp"></i>
                            <h2><span class="caption-subject font-greens bold uppercase">Blog KoPosIndo</span></h2>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1_1">
                                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 339px;"><div class="scroller" style="height: 339px; overflow: hidden; width: auto;" data-always-visible="1" data-rail-visible="0" data-initialized="1">
                                    <ul class="feeds">
                                    	@foreach ($feed->get_items(0, 5) as $item)
	                                        <li>
	                                            <div class="col1">
	                                                <div class="cont">
	                                                    <div class="cont-col1">
	                                                        <div class="label label-sm label-success">
	                                                            <i class="fa fa-bell-o"></i>
	                                                        </div>
	                                                    </div>
	                                                    <div class="cont-col2">
	                                                        <div class="desc"> <a href="{{$item->get_permalink()}}"  target="_blank">{{$item->get_title()}}</a>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
                                       		</li>
                                       	@endforeach

                                    </ul>
                                </div><div class="slimScrollBar" style="background: rgb(187, 187, 187); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 184.464px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->

            <!-- <form>
				<select onchange="showRSS(this.value)">
					<option value="">Select an RSS-feed:</option>
					<option value="Google">Google News</option>
					<option value="NBC">NBC News</option>
				</select>
			</form>
			<br>
			<div id="rssOutput">RSS-feed will be listed here...</div> -->
	    </div>
</div>

<hr/>

<div class="clear"></div>

@push('custom_scripts')
	<script type="text/javascript">
		// var adbooth_1 = $('#adbooth_1').val();
		// var Tawk_API = adbooth_1;
		// console.log(Tawk_API);
		// var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		// (function(){
		// var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		// s1.async=true;
		// s1.src='https://embed.tawk.to/59a53f7b4fe3a1168eada392/default';
		// s1.charset='UTF-8';
		// s1.setAttribute('crossorigin','*');
		// s0.parentNode.insertBefore(s1,s0);
		// })();

		function showRSS(str) {
		  if (str.length==0) { 
		    document.getElementById("rssOutput").innerHTML="";
		    return;
		  }
		  if (window.XMLHttpRequest) {
		    // code for IE7+, Firefox, Chrome, Opera, Safari
		    xmlhttp=new XMLHttpRequest();
		  } else {  // code for IE6, IE5
		    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		  xmlhttp.onreadystatechange=function() {
		    if (this.readyState==4 && this.status==200) {
		      document.getElementById("rssOutput").innerHTML=this.responseText;
		    }
		  }
		  xmlhttp.open("GET","getrss?q="+str,true);
		  xmlhttp.send();
		}
	</script>
@endpush
@endsection