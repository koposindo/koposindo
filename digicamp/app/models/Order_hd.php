<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Order_Hd extends Model{
	protected $table 		= 'orderhd';
	protected $order_status = 'digipos\models\Order_status';

    public function order_status(){
        return $this->belongsTo($this->order_status,'order_status');
    }
}